#!/bin/bash
set -e

function usage () {
cat << ENDS
Usage: $0 [options] <filename>
Blends a 2D mosaic acquired on the FEI Spirit. 

Options:
    -h | --help
        Display this help.

    -d | --debug
        Run in debug mode. All intermediate files will be kept.
ENDS
exit "$1"
}

function print_err () {
    printf "ERROR: %s\n\n" "${1}" >&2
    usage 1
}

function cleanup () {
    if [[ -f "$1" ]]; then
        rm "$1"
    fi
}

# Parse optional arguments
debug=0
while :; do
    case "${1}" in
        -h|--help)
            usage 0
            ;;
        -d|--debug)
            debug=1
            shift
            continue
            ;;
        *)
            break
    esac
    shift
done         

# Read required arguments
if [[ "$#" -ne 1 ]]; then
    print_err "Incorrect number of arguments."
fi
filename=${1}

if [[ ! -f "$filename" ]]; then
    print_err "$filename does not exist."
fi

#
# run
#
echo
echo "== 1. HACK TO FIX STACKING ISSUE =="
mrc2tif -s $filename $filename.tiff
tif2mrc $filename.tiff $filename.st

echo
echo "== 2. TRIM VOLUME =="
trimvol -nx 2040 -ny 2040 $filename.st $filename.trimmed.st
echo "... done ..."

echo
echo "== 3. CREATE PIECE LIST =="
echo "... Running: extractpieces $filename $filename.pl ..."
extractpieces $filename $filename.pl
echo "... done ..."

echo
echo "== 4. CONFIGURE SLOPPYBLEND.COM =="
echo "... Creating: $filename.sloppyblend.com ..."
echo "\$blendmont -StandardInput" >> $filename.sloppyblend.com
echo "ImageInputFile              $filename.st" >> $filename.sloppyblend.com
echo "PieceListInput              $filename.pl" >> $filename.sloppyblend.com 
echo "ImageOutputFile             $filename.bl" >> $filename.sloppyblend.com 
echo "RootNameForEdges            $filename" >> $filename.sloppyblend.com 
echo "VerySloppyMontage           1" >> $filename.sloppyblend.com 
echo "ShiftPieces                 1" >> $filename.sloppyblend.com 
echo "ReadInXcorrs                0" >> $filename.sloppyblend.com
echo "OldEdgeFunctions            0" >> $filename.sloppyblend.com 
echo "StartingAndEndingX          /" >> $filename.sloppyblend.com
echo "StartingAndEndingY          /"  >> $filename.sloppyblend.com
echo "... done ..."

echo
echo "== 5. RUN BLENDMONT =="
submfg $filename.sloppyblend.com
echo "... done ..."

#
# Cleanup 
#
cleanup $filename.tiff
cleanup $filename.pl~
cleanup $filename.st~

#
# Optional Cleanup
#
if [[ "${debug}" -eq 0 ]]; then
    cleanup $filename.ecd
    cleanup $filename.pl
    cleanup $filename.sloppyblend.com
    cleanup $filename.sloppyblend.log
    cleanup $filename.st
    cleanup $filename.trimmed.st
    cleanup $filename.xef
    cleanup $filename.yef
fi

# == FULL SLOPPYBLEND COM SCRIPT FROM ABEL BELOW==

## Command file to run BLENDMONT on an arbitrary montage such as a Navigator map 
##
## For a montage from SerialEM, first extract the piece list:
##   extractpieces filename.st filename.pl
##
## Modify all the g5a's here to match your filename
##
## Use VerySloppyMontage for stage montages 
##
#$blendmont -StandardInput
#ImageInputFile          __FILENAME__.st
#PieceListInput          __FILENAME__.pl
#ImageOutputFile         __FILENAME__.bl
#RootNameForEdges        __FILENAME__
##
## Uncomment to make an aligned stack after getting transforms
##TransformFile          g5.xf
##
## Use VerySloppyMontage instead if overlaps are particularly variable
#VerySloppyMontage   1   
#ShiftPieces     1   
##
## Change this to 1 after the first run if you fix edges in midas
#ReadInXcorrs    0   
#OldEdgeFunctions        0
#StartingAndEndingX      /   
#StartingAndEndingY      /   
